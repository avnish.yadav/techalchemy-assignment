import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  signupForms!: FormGroup;
  submitted: boolean = false;

  constructor(private formbuilder: FormBuilder, private router: Router) {}

  ngOnInit(): void {
    this.signupForms = this.formbuilder.group({
      email: [
        '',
        [Validators.required, Validators.maxLength(40), Validators.email],
      ],
      password: ['', [Validators.required, Validators.maxLength(20)]],
    });
  }
  get values() {
    return this.signupForms.controls;
  }
  signupSubmit() {
    this.submitted = true;
  }
}
