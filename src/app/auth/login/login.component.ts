import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForms!: FormGroup;
  submitted: boolean = false;
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.loginForms = this.formbuilder.group({
      email: [
        '',
        [Validators.required, Validators.maxLength(40), Validators.email],
      ],
      password: ['', [Validators.required, Validators.maxLength(20)]],
    });
  }
  get values() {
    return this.loginForms.controls;
  }
  loginSubmit() {
    this.submitted = true;
    if (this.loginForms.valid) {
      if (
        this.loginForms.controls['email'].value ===
          'avnish.yadav@appventurez.com' &&
        this.loginForms.controls['password'].value === 'qwerty123'
      ) {
        localStorage.setItem('IslogedIn', 'true');
        // sessionStorage.setItem('isLogin', 'yes');
        this.toastr.success('Login Successful!');
        this.router.navigate(['/dashboard']);
      } else {
        this.toastr.error('Enter valid credentials!');
      }
    } else {
      this.toastr.error('Enter all credentials!');
    }
  }
}
