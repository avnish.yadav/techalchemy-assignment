import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewRoutingModule } from './view-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HotTopicsComponent } from './hot-topics/hot-topics.component';
import { LatestNewsComponent } from './latest-news/latest-news.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    DashboardComponent,
    NavbarComponent,
    HotTopicsComponent,
    LatestNewsComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    ViewRoutingModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    HttpClientModule,
  ],
})
export class ViewModule {}
