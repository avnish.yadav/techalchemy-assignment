import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
// import { HttpClient,HttpErrorResponse } from "@angular/common/http";
import { CommonServiceService } from '../services/common-service.service';

@Component({
  selector: 'app-latest-news',
  templateUrl: './latest-news.component.html',
  styleUrls: ['./latest-news.component.scss'],
})
export class LatestNewsComponent implements OnInit {
  boxes: any;

  constructor(public httpClient: HttpClient) {
    //fecthing news data stored in assets folder json file
    this.getLocalJson('assets/news.json');

    //  var getData: any = this.commonservice.getLocalJson('assets/news.json');
    //    console.log(getData)
    //  this.boxes = getData.content;
  }

  getLocalJson(path: any) {
    this.httpClient.get<any>(path).subscribe((data) => {
      (this.boxes = data.content), console.log(data);
    });
  }

  onScroll(e: any) {
    if (this.boxes.length >= 100) {
      console.log('No more items');
      return;
    }
    console.log('scrolled!!', e);
    const moreBoxes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.boxes = [...this.boxes, ...moreBoxes];
  }

  ngOnInit(): void {}
}
