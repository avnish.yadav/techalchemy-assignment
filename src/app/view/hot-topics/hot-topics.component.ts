import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hot-topics',
  templateUrl: './hot-topics.component.html',
  styleUrls: ['./hot-topics.component.scss'],
})
export class HotTopicsComponent implements OnInit {
  weather: any;
  mobilereport: any;
  constructor() {
    this.weather = [
      {
        day: 'Thursday',
        img: './../../../assets/sun.png',
        temp: '13',
        temp2: '11',
      },
      {
        day: 'Friday',
        img: './../../../assets/sun.png',
        temp: '14  ',
        temp2: '10',
      },
      {
        day: 'Saturday',
        img: './../../../assets/sun.png',
        temp: '17',
        temp2: '14',
      },
      {
        day: 'Sunday',
        img: './../../../assets/sun.png',
        temp: '19',
        temp2: '17',
      },
      {
        day: 'Monday',
        img: './../../../assets/sun.png',
        temp: '16',
        temp2: '12',
      },
    ];

    this.mobilereport = [
      {
        day: 'Wed',
        img: './../../../assets/sun.png',
        temp: '16',
      },
      {
        day: 'Thu',
        img: './../../../assets/sun.png',
        temp: '13',
      },
      {
        day: 'Fri',
        img: './../../../assets/sun.png',
        temp: '14',
      },
      {
        day: 'Sat',
        img: './../../../assets/sun.png',
        temp: '17',
      },
      {
        day: 'Sun',
        img: './../../../assets/sun.png',
        temp: '19',
      },
    ];
  }

  ngOnInit(): void {}
}
