import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanLoad,
  Route,
  CanActivateChild,
} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate, CanActivateChild {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let url = state.url;
    return this.checkLogin(url);
  }

  // canLoad(route: Route): boolean {
  //   let url = route.path;
  //   return this.checkLogin(url);
  // }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return this.canActivate(route, state);
  }

  checkLogin(url: string) {
    const IslogedIn = localStorage.getItem('IslogedIn');
    // const token = localStorage.getItem('session');

    if (IslogedIn) {
      return true;
    } else {
      this.router.navigate(['/']);
    }

    this.router.navigate(['/login'], { queryParams: { attemptedUrl: url } });
    return false;
  }
}
