import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  constructor(public httpClient: HttpClient) { 
    
  }

  getLocalJson(path: any){
    this.httpClient.get<any>(path).subscribe((data: any)=>
     {
        return data;
       
    }
   )
  }
}
